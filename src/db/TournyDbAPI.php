<?php

/**
 * Tourny API for database communication with mysql from javascript
**/


session_start();

include ('./db.class.php');
include ('./TournyEngine.php');

header('Content-type: application/json');

//Load mysql
$dbhost         = 'localhost';
$dbname         = 'cmoe_dk';
$dbuser         = 'USER';
$dbpasswd       = 'PASS';

//Switch to localhost, if applicaple
$whitelist = array('localhost', '127.0.0.1');

if(in_array($_SERVER['HTTP_HOST'], $whitelist)){
        $dbhost = 'localhost';
}

//Connect to database
$db = new mysql_db($dbname, $dbhost, $dbuser, $dbpasswd);

//Tourny.JS will refuse to accept anything, but the expected data. Gather the data in this variable for easier overview of what is returned.
$returnData = array();

//Gather post data
$data = $_POST;

//Values that are commonly returned
$data['ERROR'] 		= '';
$data['LOGGED_IN']	= false;

//Check if user is already logged in
$userIsLoggedIn = isset($_SESSION['LOGGED_IN']) && $_SESSION['LOGGED_IN'];

if(isset($data['REQUEST_TYPE'])) {

	$requestType = $data['REQUEST_TYPE'];
}
else
{
	$requestType = 'unknown';
}


switch($requestType) {

	case 'login':
		$data['IS_ADMIN'] = false;
		
		
		if($userIsLoggedIn) 
		{
			$data['USERNAME'] 	= $_SESSION['USERNAME'];
			$data['IS_ADMIN'] 	= $_SESSION['IS_ADMIN'];
			$data['LOGGED_IN'] 	= $_SESSION['LOGGED_IN'];
		}
		else
		{
			$username = mysql_real_escape_string($data['USERNAME']);
			
			if(!isset($data['USERNAME']) || !isset($data['PASSWORD']))
			{
				$data['ERROR'] = 'No username or password in POST.';
				break;
			}
			
			//Tourny.js tries a login attempt at initiation to avodi logging out in case of a page refresh. 
			//It will use this specific username and password combination, so skip the database connection with this combination
			if($data['USERNAME'] !== 'anonymous' && $data['PASSWORD'] !== '')
			{
				if($db->exists('tourny_users', 'username="' . $username . '"'))
				{
					$result = $db->fields('tourny_users', array('password', 'level'), 'username="' . $username . '"');
					
					if(md5($data['PASSWORD']) == $result['password'])
					{
						$data['LOGGED_IN'] 		= $_SESSION['LOGGED_IN'] = true;
						
						$_SESSION['USERNAME'] 	= $username;
						$data['IS_ADMIN'] 		= $_SESSION['IS_ADMIN'] = $result['level'] < 1 ? true : false;
					}
					else
					{
						$data['ERROR'] = 'Wrong username or password';
					}
				}
				else
				{
					$data['ERROR'] = 'Wrong username or password';
				}
			}
		}
		
		
		$returnData = array(
			'ERROR' 	=> $data['ERROR'],
			'USERNAME'	=> $data['USERNAME'],
			'IS_ADMIN'	=> $data['IS_ADMIN'],
			'LOGGED_IN'	=> $data['LOGGED_IN']
		);
	break;
	
	case 'logout':
		// Unset all of the session variables.
		$_SESSION = array();

		// If it's desired to kill the session, also delete the session cookie.
		// Note: This will destroy the session, and not just the session data!
		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}

		// Finally, destroy the session.
		session_destroy();
				
		$returnData = array(
			'ERROR' 	=> $data['ERROR'],
			'LOGGED_IN'	=> false
		);
		
	break;
	
	case 'load' :
		$result = array();
	
		if($data['TARGET'] !== 'editor') {
		
			$tables = array('tourny_countries', 'tourny_leagues', 'tourny_clubs', 'tourny_players');
			
			$countries 	= $db->rows('SELECT * FROM tourny_countries');
			
			foreach($countries as $country) {
				$result['countries'][$country['id']] = $country;
				$leagues = $db->rows('SELECT * FROM tourny_leagues WHERE country_id = ' . $country['id']);
				
				foreach($leagues as $league) {
					$result['countries'][$country['id']]['leagues'][$league['id']] = $league;
					$clubs = $db->rows('SELECT * FROM tourny_clubs WHERE league_id = ' . $league['id']);
					
					foreach($clubs as $club) {
						$result['countries'][$country['id']]['leagues'][$league['id']]['clubs'][$club['id']] = $club;
						$players = $db->rows('SELECT * FROM tourny_players WHERE club_id = ' . $club['id']);
						
						foreach($players as $player) {
							$result['countries'][$country['id']]['leagues'][$league['id']]['clubs'][$club['id']]['players'][$player['id']] = $player;
						}
					}
				}
			}
			
		}
		else
		{
			$table =  mysql_real_escape_string($data['TABLE']);
			
			$title = mysql_real_escape_string($data['SELECT_ID']);
			
			$result[$title] = $db->rows('SELECT * FROM ' . $table);
		}
			
		
		$returnData = array(
			'ERROR' => $data['ERROR'],
			'DATA'	=> $result
		);
	break;
	
	case 'new' :
		if($_SESSION['LOGGED_IN']) {
			$table = 'tourny_' . mysql_real_escape_string($data['DATA']['TYPE']);
			
			$fields = $data['DATA']['FIELDS'];
			
			$safeFields = array_map('mysql_real_escape_string', $fields);
			
			if(!$db->insert($table, array($safeFields)))
			{
				$error = $db->error();
				
				if (0 === strpos($error, 'Duplicate')) {
					$data['ERROR'] = 'Country name or URL already exists in database';
				}
				else
				{
					$data['ERROR'] = $error;
				}
			}
		} 
		else {
			$data['ERROR'] = 'User is not logged in';
		}
		$returnData = array(
			'ERROR' => $data['ERROR']
		);
	break;
	
	case 'unknown':
		$returnData = array(
			'ERROR' => 'Unknown request type'
		);
	break;
}

echo json_encode($returnData);

?>
