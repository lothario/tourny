<?php
class mysql_db {
	var $result;
	var $dbCon;
	var $sql;
	
	public function __construct($dbName, $dbHost, $dbUser, $dbPass) 
	{
		$this->dbCon 		= mysql_connect($dbHost,$dbUser,$dbPass);
		$this->dbSelect 	= mysql_select_db($dbName, $this->dbCon);
	}
	
	public function delete($table, $field, $value) 
	{
		$this->sql = "DELETE FROM $table
				WHERE $field = '$value'";

		return $this->result = $this->query();
	}
	
	public function insert($table, $data)
	{
		foreach($data as $insert)
		{
			$this->sql = 'INSERT INTO ' . $table . '(';
		
			foreach($insert as $field => $value)
			{
				$this->sql .= ' ' . $field . ',';
			}
			
			$this->sql = substr($this->sql, 0, -1);
		
			$this->sql .= ') VALUES (';
			
			foreach($insert as $value)
			{
				$value = $this->escape($value);
				$this->sql .= '"'.$value.'",';
			}
			
			$this->sql = substr($this->sql, 0, -1);
			
			$this->sql .= ')';

			if(!($this->query()))
			{
				return false;
			}
		}
		
		return true;
	}
	
	public function update($table, $data, $where)
	{
		foreach($data as $update)
		{
			$this->sql = 'UPDATE ' . $table . ' SET ';
			
			foreach($update as $field => $value)
			{
				$value = $this->escape($value);
				if(is_numeric($value))
				{
					$this->sql .= ' ' . $field . ' = ' . $value . ',';
				}
				else
				{
					$this->sql .= ' ' . $field . ' = "' . $value . '",';
				}
			}
			
			$this->sql = substr($this->sql, 0, -1);
		
			if($where != '')
			{
				$this->sql .= ' WHERE ' . $where;
			}
			
			if(!($this->query()))
			{
				return false;
			}
		}
		
		return true;
	}
	
	public function query($sql = '')
	{
		if($sql != '')
		{
			$this->sql = $sql;
		}
		
		return $this->result = mysql_query($this->sql);
	}
	
	public function fields($table, $fields, $where)
	{
		$field_sql = '';
		
		foreach ($fields as $field)
		{
			$field_sql .= ' ' . $field . ',';
		}
		
		$field_sql = substr($field_sql, 0, -1);
	
		if($where !== '')
		{
			$where = ' WHERE ' . $where; 
		}
		
		$this->sql = 'SELECT ' . $field_sql . ' FROM ' . $table . $where . ' LIMIT 1';
		
		$this->query();
		
		$row = mysql_fetch_array($this->result);
		
		return $row;
	}
	
	public function exists($table, $where)
	{
		$this->sql = 'SELECT * FROM ' . $table . ' WHERE ' . $where . ' LIMIT 1';
		
		if($this->query())
		{
			
			return $this->num_rows() > 0;
		}
		else
		{
			return false;
		}
	}
	
	public function rows($sql)
	{
		$this->sql = $sql;
		$this->query();
		
		$rows = array();
		
		while ($row = mysql_fetch_assoc($this->result))
		{
			$rows[] = $row;
		}
		
		return $rows;
	}
	
	public function num_rows()
	{
		return mysql_num_rows($this->result);
	}
	
	public function last_id()
	{
		return mysql_insert_id();
	}
	
	public function escape($value, $isFloat = false)
	{
		if($isFloat)
		{
			$value = (float) $value;
		}
		else
		{
			$value = mysql_real_escape_string($value);
		}
		return $value;
	}
	
	public function error()
	{
		return mysql_error($this->dbCon) . "<br />" . $this->sql;
	}
}
?>