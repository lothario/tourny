var Tourny = (function(){
    //Settings
    var loggedIn            = false;
    var menuTimeOut         = 150;
    var underMenuTimeOut    = 150;
    var subMenuTimeOut      = 150;
    var hoverTimeOut        = 800;
    var contentTimeOut      = 200;

    //Menu classes and ids
    var menuMainId      = '#tourny-menu';
    var subMenuClass    = '.submenu';
    var underMenuClass  = '.undermenu';
    var toggleBoxClass  = '.toggle-box';
    var menuItemClass   = '.item';
    var menuTitleClass  = '.title';

    //Login classes and ids
    var loginButtonId       = '#tourny-login-link';
    var loginActiveUserId   = '#tourny-active-user';
    var loginUsernameId     = '#tourny-username';
    var loginPasswordId     = '#tourny-password';
    var loginMainId         = '#tourny-login-box';
    var loginDisabledClass  = '.disabled';
    var loginSelectedClass  = '.selected';

    //Editor classes and ids
    var editorLinkId    = '#tourny-editor';
    var editorContentId = '#tourny-data-editor';
    var editorHeaderClass = '.tourny-editor-header';
    var editorClass         = '.tourny-editor';
    var editorNewClass         = '.tourny-editor-new';

    //Content classes and ids
    var contentDataId       = '#tourny-data-content';
    var contentClass        = '.tourny-content';
    var contentTableId      = '#tourny-table';
    var contentViewerId     = '#tourny-viewer';

    //JS changeable classes and ids
    var JSLoginRequiredClass    = '.js-login-required';
    var JSAdminRequiredClass    = '.js-admin-required';
    var JSLoginSubmit           = '.js-login';
    var JSCreateNew             = '.js-new';
    var JSClose                 = '.js-close';

    //Database variables
    var TournyDBAPIUrl     = './db/TournyDbAPI.php';

    var menu = {
        hideAll: function(){
            $(subMenuClass).hide();
            $(underMenuClass).hide();
        },

        init: function() {
            menu.hideAll();
            $(toggleBoxClass).hide();


            //Main menu items have a small delay to account for mouse movement intent
            $(menuItemClass).hoverIntent({
                over: function(){
                    $(this).children(subMenuClass).show(subMenuTimeOut);
                },
                //Hide undermenus as well, when moving away from submenus
                out: function(){
                    $(this).children(subMenuClass).hide(subMenuTimeOut);
                    $(underMenuClass).slideUp(underMenuTimeOut);
                },
                timeout: hoverTimeOut
            });

            //Make it possible to expand the menu by clicking the main menu item title, if the hover does not work
            $(menuMainId + '>' + menuItemClass + ' ' + menuTitleClass).click(function(){
                $(this).parent(menuItemClass).children(subMenuClass).toggle(subMenuTimeOut);
            });

            $(subMenuClass + ' ' + menuItemClass).hoverIntent({
                over: function(){
                    $(this).children(underMenuClass).slideDown(underMenuTimeOut);
                },
                //Keep undermenu open, until submenu is closed
                out: function(){},
                timeout: hoverTimeOut
            });

            $(contentViewerId).click(function(){
                $(this).closest(subMenuClass).slideUp(subMenuTimeOut);
                $(editorContentId).slideUp(contentTimeOut);
                content.load('viewer', '', '');
            });

            $(editorLinkId).click(function(){
                $(this).closest(subMenuClass).slideUp(subMenuTimeOut);
                $(editorContentId).slideDown(contentTimeOut);
                $(contentDataId).slideUp(contentTimeOut);
            });
        }
    };

    var content = {
      data: {},
      loaded: [],

      init: function() {
        $(editorContentId).hide();
        $(contentDataId).hide();

        $(JSClose).click(function(){
            $(this).parent('div').slideUp(contentTimeOut);
        });

          //Load select content on hover
        $('select').hover(function() {
            if(!content.loaded[$(this).attr('id')]) {
                content.load('editor', $(this).attr('data-table'), $(this).attr('id'));
            }
        });

        $(JSCreateNew).on('click submit', function() {
            var breakSubmit = false;

            var formData = {
                TYPE: $(this).closest(editorClass).children(editorHeaderClass).html().toLowerCase(),
                FIELDS: {},
                INPUT_IDS: [],
                SELECT_IDS: []
            };

            //Check that each input has a value
            $(this).parent(editorNewClass).children('div').children('input').each(function() {
                if( $(this).val() === '') {
                    alert('Please fill in all fields');
                    $(this).focus();
                    breakSubmit = true;
                    formData.FIELDS = {};
                    return false;
                } else {
                    //Check if the url is valid
                    if($(this).attr('data-field').indexOf('url') !== -1) {
                        UrlExists($(this).val(), function(status){
                            if(status !== 200){
                                alert('The url could not be resolved. Please correct it.');
                                formData.FIELDS = {};
                                breakSubmit = true;
                                return false;
                            }
                        });
                    }

                    if(breakSubmit) {
                        return false;
                    }

                    formData.FIELDS[$(this).attr('data-field')] = $(this).val();
                    formData.INPUT_IDS.push($(this).attr('id'));
                }
            });

            if(breakSubmit) {
                return false;
            }

            //Check if a select input exists and have a permitted value
            var selectTag = $(this).parent(editorNewClass).find('select');

            if(selectTag.length > 0) {
                var selectedOption = selectTag.children('option:selected');

                if(isNaN(selectedOption.val())) {
                    alert('Please select an option from the drop-down menu');
                    return false;
                } else {
                    formData.FIELDS[selectTag.attr('data-field')] = parseInt(selectedOption.val());
                    formData.SELECT_IDS.push(selectTag.attr('id'));
                }
            }
            content.new(formData);
        });
      },

      hideAll: function() {
        $(contentClass).slideUp(contentTimeOut);
      },

      load: function(target, table, selectId) {
          var postData = {REQUEST_TYPE: 'load', TARGET: target, TABLE: table, SELECT_ID: selectId};

          $.ajax({
              type: "POST",
              url: TournyDBAPIUrl,
              data: postData,
              success: function(JSONData) {
                  var expectedKeys = {ERROR: '', DATA: {}};
                  var finalData = processJSONData(JSONData, expectedKeys);

                  if(jQuery.isEmptyObject(finalData)){
                      return;
                  }

                  console.log(finalData);

                  if(jQuery.isEmptyObject(finalData.DATA)){
                      alert('No data exists');
                      return false;
                  } else {

                    content.data = finalData.DATA;

                    if(target === 'viewer') {

                        var countryHTML = '<ul>';
                        for(var country in content.data.countries) {

                            if (content.data.countries.hasOwnProperty(country)) {
                                countryHTML += '<li class="tourny-data-country">'
                                    + '<img src="' + content.data.countries[country].flag_url + '" />'
                                    + content.data.countries[country].name
                                    + '<span class="tourny-content-edit-link js-login-required">edit</span></li>';
                            }

                        }
                        countryHTML += '</ul>';

                        $('#tourny-data-countries').html(countryHTML);

                        if(!loggedIn){
                            $('.tourny-content-edit-link').hide();
                        }

                        $(contentDataId).slideDown(contentTimeOut);
                    } else {
                        content.loaded[selectId] = true;
                        $('#' + selectId + ' option').not('.js-default').remove();

                        for(var key in content.data) {
                            for(var j=0; j < content.data[key].length; j++) {
                                $('#' + key).append('<option value="' + content.data[key][j].id + '">' + content.data[key][j].name + '</option>');
                            }

                        }
                    }
                  }
              },
              dataType: 'json',
              error: function(jqXHR, exception){
                  alertError(jqXHR, exception);

                  console.log(jqXHR.responseText);
              }
          });
      },

      new : function(formData) {

          var postData = {REQUEST_TYPE: 'new', DATA: formData};
          $.ajax({
              type: "POST",
              url: TournyDBAPIUrl,
              data: postData,
              success: function(JSONData) {
                  var expectedKeys = {ERROR: ''};
                  var finalData = processJSONData(JSONData, expectedKeys);

                  if(jQuery.isEmptyObject(finalData)){
                      return false;
                  }

                  if(finalData.ERROR === ''){
                      if(formData.FIELDS.name) {
                          alert(formData.FIELDS.name + ' created successfully');
                      } else {
                          alert('Item created successfully');
                      }



                      for (var i = 0; i < formData.INPUT_IDS.length; i++) {
                          var id = formData.INPUT_IDS[i];

                          $('#' + id).val('');
                      }

                      for (i = 0; i < formData.SELECT_IDS.length; i++) {
                          id = formData.SELECT_IDS[i];

                          $('#' + id).prop('selectedIndex',0);;
                      }

                      $('option').not('.js-default').remove();
                      content.loaded = [];

                  } else {
                     alert(finalData.ERROR);
                      return false;
                  }
              },
              dataType: 'json',
              error: function(jqXHR, exception){
                  alertError(jqXHR, exception);

                  console.log(jqXHR.responseText);
              }
          });
      }
    };

    var user = {
        data: {},

        init: function() {
            disableLoginFeatures();

            //Checks if the user is already logged in on the php side
            user.login();

            $(loginButtonId).click(function(){
                if(loggedIn) {
                    $(loginButtonId).html('login');
                    $(loginActiveUserId).html('');
                    $(loginUsernameId + ', ' + loginPasswordId).val('');
                    disableLoginFeatures();
                    user.logout();
                } else {
                    //Make menu hover style permanent
                    $(this).parent('.item').toggleClass(loginSelectedClass.substr(1));

                    $(loginMainId).slideToggle(menuTimeOut);
                    $(loginUsernameId).focus();
                }

            });

            //Go to next field on enter
            $(loginUsernameId).keydown(function (e){
                if(e.keyCode == 13){
                    $(loginPasswordId).focus();
                }
            });

            $(loginPasswordId).keydown(function (e){
                if(e.keyCode == 13){
                    $(JSLoginSubmit).submit();
                }
            });

            $(JSLoginSubmit).on('click submit', function(){

                user.login($(loginUsernameId).val(), $(loginPasswordId).val());

            });
        },

        loginSuccess: function() {
            loggedIn = true;
            enableLoginFeatures();

            menu.hideAll();
            $(loginMainId).slideUp();

            $(loginButtonId).html('logout');
            $(loginActiveUserId).html(user.data.USERNAME);
            $(loginMainId).parent(menuItemClass).removeClass(loginSelectedClass.substr(1));
            $(JSLoginSubmit).val('Login');
        },

        login: function() {

            var postData = {};

            if(arguments.length == 2) {
                var username = arguments[0];
                var password = arguments[1];

                if(username === '') {
                    alert('Please fill in username');
                    $(loginUsernameId).focus();
                    return;
                }

                if(password === '') {
                    alert('Please enter a password');
                    $(loginPasswordId).focus();
                    return;
                }

                postData  = {REQUEST_TYPE: 'login', USERNAME: username, PASSWORD: password};
                user.disableLogin();

                $(JSLoginSubmit).val('Please wait...');
            } else {
                postData =  {REQUEST_TYPE: 'login', USERNAME: 'anonymous', PASSWORD: ''};
            }

            $.ajax({
                type: "POST",
                url: TournyDBAPIUrl,
                data: postData,
                success: function(JSONData) {
                    var expectedKeys = {ERROR: '', LOGGED_IN: '', USERNAME: '', IS_ADMIN: 0};
                    var finalData = processJSONData(JSONData, expectedKeys);

                    if(jQuery.isEmptyObject(finalData)){
                        return;
                    }

                    console.log(finalData);

                    if(finalData.LOGGED_IN){
                        user.data = finalData;
                        user.loginSuccess();
                    }

                    if(finalData.ERROR !== '') {
                        alert(JSONData.ERROR);
                        user.enableLogin();
                        $(loginUsernameId).focus();

                        $(JSLoginSubmit).val('login');
                    }
                },
                dataType: 'json',
                error: function(jqXHR, exception){
                    alertError(jqXHR, exception);

                    alert(jqXHR.responseText);

                    user.enableLogin();
                    $(loginUsernameId).focus();

                    $(JSLoginSubmit).val('login');
                }
            });
        },

        logout: function() {

            var postData  = {REQUEST_TYPE: 'logout'};

            $.ajax({
                type: "POST",
                url: TournyDBAPIUrl,
                data: postData,
                success: function(JSONData) {
                    var expectedKeys = {ERROR: '', LOGGED_IN: ''};

                    var finalData = processJSONData(JSONData, expectedKeys);

                    if(jQuery.isEmptyObject(finalData)){
                        return;
                    }
                    loggedIn = false;

                    disableLoginFeatures();
                    user.enableLogin();
                    user.data = {};

                    console.log(finalData);

                    if(finalData.ERROR !== '') {
                        alert(JSONData.ERROR);
                    }
                },
                dataType: 'json',
                error: function(jqXHR, exception){
                    alertError(jqXHR, exception);

                    alert(jqXHR.responseText);

                    user.enableLogin();
                    $(loginUsernameId).focus();

                    $(JSLoginSubmit).val('login');
                }
            });
        },

        disableLogin: function () {
            $(JSLoginSubmit + ', ' + loginUsernameId + ', ' + loginPasswordId).prop('disabled', true);
            $(JSLoginSubmit + ', ' + loginUsernameId + ', ' + loginPasswordId).addClass(loginDisabledClass.substr(1));
        },

        enableLogin: function () {
            $(JSLoginSubmit + ', ' + loginUsernameId + ', ' + loginPasswordId).prop('disabled', false);
            $(JSLoginSubmit + ', ' + loginUsernameId + ', ' + loginPasswordId).removeClass(loginDisabledClass.substr(1));
        }
    };

    function disableLoginFeatures() {
        $(JSLoginRequiredClass + ', ' + JSAdminRequiredClass).hide();
    }

    function enableLoginFeatures() {
        $(JSLoginRequiredClass).show();

        if(user.data.IS_ADMIN) {
            $(JSAdminRequiredClass).show();
        }

    }

    function alertError(jqXHR, exception) {
        if (jqXHR.status === 0) {
            alert('Error. Not connected.\n Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Error. Requested page not found. [404]');
        } else if (jqXHR.status == 500) {
            alert('Error. Internal Server Error [500].');
        } else if (exception === 'parsererror') {
            alert('Error. Requested JSON parse failed.');
        } else if (exception === 'timeout') {
            alert('Error. Time out error.');
        } else if (exception === 'abort') {
            alert('Error. Ajax request aborted.');
        } else {
            alert('Error. Uncaught Error.\n' + jqXHR.responseText);
        }
    }

    function processJSONData(object, expectedObj) {
        var aKeys = Object.keys(object).sort();
        var bKeys = Object.keys(expectedObj).sort();
        var isIdentical =  JSON.stringify(aKeys) === JSON.stringify(bKeys);

        var resultObject = {};

        if(isIdentical) {
            for (var property in object) {
                if (object.hasOwnProperty(property)) {
                    resultObject[property] = object[property];
                }
            }
        } else {
            alert('Tourny API did not return expected keys.');
            return;
        }

        return resultObject;
    }

    function UrlExists(url, cb){
        jQuery.ajax({
            url:      url,
            dataType: 'text',
            type:     'GET',
            async: false,
            complete:  function(xhr){
                if(typeof cb === 'function')
                    cb.apply(this, [xhr.status]);
            },
            timeout: 5000,
            error: function(x, t, m) {
                if(t==="timeout") {
                    alert("Server timeout. Please try again.");
                }
            }
        });
    }

    return {
        init: function() {
            menu.init();
            user.init();
            content.init();
        },

        menuTimeOut:        menuTimeOut,
        underMenuTimeOut:   underMenuTimeOut,
        subMenuTimeOut:     subMenuTimeOut,
        hoverTimeOut:       hoverTimeOut
    }
})();


